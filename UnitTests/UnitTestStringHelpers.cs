﻿using LectureScheduleManagementSystem.Helpers;
using Xunit;

namespace UnitTests
{
	public class UnitTestStringHelpers
	{
		[Fact]
		public void TestRandomStringNullOrEmpty()
		{
			string result = StringHelpers.RandomString(10);

			Assert.True(!string.IsNullOrEmpty(result));
		}

		[Fact]
		public void TestConvertStringArrayToString()
		{
			string result = StringHelpers.ConvertStringArrayToString(new string[] { "Test", "Test1" });

			Assert.True(!string.IsNullOrEmpty(result));
			Assert.Equal("Test Test1", result);
		}

		[Fact]
		public void TestRandomStringLength()
		{
			string result = StringHelpers.RandomString(10);

			int expected = 10;
			Assert.Equal(expected, result.Length);
		}
	}
}
