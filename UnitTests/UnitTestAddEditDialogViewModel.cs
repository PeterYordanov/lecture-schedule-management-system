﻿using LectureScheduleManagementSystem.ViewModels;
using Moq;
using System;
using Xunit;

namespace UnitTests
{
	public class UnitTestAddDialogViewModel
	{
		[Fact]
		public void TestCommandCanExecute()
		{
			var mainViewModel = new MainViewModel();
			var addDialogViewModel = new AddDialogViewModel(mainViewModel);

			Assert.False(addDialogViewModel.CommandCanExecute());
		}

		[Fact]
		public void TestIsBusy()
		{
			var mainViewModel = new MainViewModel();
			var addDialogViewModel = new AddDialogViewModel(mainViewModel);

			Assert.False(addDialogViewModel.IsBusy);
		}

		[Fact]
		public void TestRemoveSubjectCellCanExecute()
		{
			var mainViewModel = new MainViewModel();
			var addDialogViewModel = new AddDialogViewModel(mainViewModel);

			Assert.True(addDialogViewModel.RemoveSubjectCellCommand.CanExecute(""));
		}

		[Fact]
		public void TestAddCanExecute()
		{
			var mainViewModel = new MainViewModel();
			var addDialogViewModel = new AddDialogViewModel(mainViewModel);

			Assert.False(addDialogViewModel.AddCommand.CanExecute(""));
		}

		[Fact]
		public void TestApplyChange()
		{
			var mainViewModel = new MainViewModel();
			var addDialogViewModel = new AddDialogViewModel(mainViewModel);

			Assert.False(addDialogViewModel.ApplyChangeCommand.CanExecute(""));
		}

		[Fact]
		public void TestAddSubjectCellCanExecute()
		{
			var mainViewModel = new MainViewModel();
			var addDialogViewModel = new AddDialogViewModel(mainViewModel);

			Assert.True(addDialogViewModel.AddSubjectCellCommand.CanExecute(""));
		}
	}
}
