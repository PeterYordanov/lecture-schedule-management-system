﻿using LectureScheduleManagementSystem.Logging;
using System;
using System.IO;
using Xunit;

namespace UnitTests
{
	public class UnitTestLogging
	{
		[Fact]
		public void TestLogInfo()
		{
			Logger logger = new Logger();

			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);

				logger.LogInfo(new LogModel 
				{
					Message = "testInfo",
					LogType = LogType.Information
				});

				string consoleOutput = stringWriter.ToString();

				Assert.True(!string.IsNullOrEmpty(consoleOutput));
				Assert.Contains("testInfo", consoleOutput);
				Assert.Contains("Info", consoleOutput);
			}
		}

		[Fact]
		public void TestLogError()
		{
			Logger logger = new Logger();

			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);

				logger.LogError(new LogModel
				{
					Message = "testError",
					LogType = LogType.Error
				});

				string consoleOutput = stringWriter.ToString();

				Assert.True(!string.IsNullOrEmpty(consoleOutput));
				Assert.Contains("testError", consoleOutput);
				Assert.Contains("Err", consoleOutput);
			}
		}

		[Fact]
		public void TestLogWarning()
		{
			Logger logger = new Logger();

			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);

				logger.LogWarning(new LogModel
				{
					Message = "testWarn",
					LogType = LogType.Warning
				});

				string consoleOutput = stringWriter.ToString();

				Assert.True(!string.IsNullOrEmpty(consoleOutput));
				Assert.Contains("testWarn", consoleOutput);
				Assert.Contains("Warn", consoleOutput);
			}
		}

		[Fact]
		public void TestLogFatal()
		{
			Logger logger = new Logger();

			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);

				logger.LogFatal(new LogModel
				{
					Message = "testFatal",
					LogType = LogType.Fatal
				});

				string consoleOutput = stringWriter.ToString();

				Assert.True(!string.IsNullOrEmpty(consoleOutput));
				Assert.Contains("testFatal", consoleOutput);
				Assert.Contains("Fatal", consoleOutput);
			}
		}
	}
}
