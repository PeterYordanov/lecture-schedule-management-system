﻿using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.Helpers;
using System.Collections.Generic;
using Xunit;

namespace UnitTests
{
	public class UnitTestIEnumerableExtensions
	{
		[Fact]
		public void TestIsNullOrEmptyTrue()
		{
			IEnumerable<string> list = new List<string>();

			Assert.True(list.IsNullOrEmpty());
		}

		[Fact]
		public void TestIsNullOrEmptyFalse()
		{
			IEnumerable<string> list = new List<string> { "test", "tests" };
			Assert.False(list.IsNullOrEmpty());
		}

		[Fact]
		public void TestDistinctBy()
		{
			IEnumerable<string> list = new List<string> { "test", "tests", "test", "testt", "testt" };
			string result = list.DistinctBy(x => x.Length).ToString();
			Assert.False(string.IsNullOrEmpty(result));
		}
	}
}
