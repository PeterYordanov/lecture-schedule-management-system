﻿using LectureScheduleManagementSystem.Extensions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xunit;

namespace UnitTests
{
	public class UnitTestObservableCollectionExtensions
	{
		[Fact]
		public void TestDistinctBy()
		{
			ObservableCollection<MockObject> list = new ObservableCollection<MockObject>
			{
				new MockObject
				{
					FirstName = "Peter",
					LastName = "Yordanov",
				},
				new MockObject
				{
					FirstName = "Falco",
					LastName = "Girgis",
				},
				new MockObject
				{
					FirstName = "Peter",
					LastName = "Yordanov",
				},
				new MockObject
				{
					FirstName = "Peter",
					LastName = "Yordanov",
				}
			};

			int result = list.DistinctBy(x => x.FirstName).Count;

			int expected = 2;

			Assert.Equal(expected, result);
		}
	}
}
