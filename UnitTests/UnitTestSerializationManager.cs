﻿using LectureScheduleManagementSystem.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
	public class MockObject
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public int Age { get; set; }
	};

	public class UnitTestSerializationManager
	{
		[Fact]
		public void TestSerialize()
		{
			ISerializationManager serializationManager = new SerializationManager();

			string result = serializationManager.Serialize(new MockObject
			{
				FirstName = "Falco",
				LastName = "Girgis",
				Age = 30
			});

			Assert.Contains("Falco", result);
			Assert.Contains("30", result);
			Assert.Contains(":", result);
			Assert.Contains("{", result);
			Assert.Contains("}", result);
		}

		[Fact]
		public void TestDeserialize()
		{
			ISerializationManager serializationManager = new SerializationManager();

			string serialized =
			@"{  
			'FirstName':'Falco',  
			'LastName':'Girgis',
			'Age': 30
			}";

			MockObject result = serializationManager.Deserialize<MockObject>(serialized);

			Assert.Equal("Falco", result.FirstName);
			Assert.Equal("Girgis", result.LastName);
			Assert.Equal(30, result.Age);
		}
	}
}
