﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace LectureScheduleManagementSystem.Extensions
{
	/// <summary>
	/// 
	/// </summary>
	static public class StringExtensions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string Clean(this string str)
		{
			try
			{
				return Regex.Replace(str, @"[^\w\.@&-]", "",
									 RegexOptions.None, TimeSpan.FromSeconds(1.5));
			}
			catch (RegexMatchTimeoutException)
			{
				return String.Empty;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string ReplaceInvalidChars(this string str)
		{
			return string.Join("_", str?.Split(Path.GetInvalidFileNameChars())).Replace('(', '_').Replace(')', '_') //' , ~, #, !, $, %, ^, 
																			   .Replace('&', '_').Replace(',', '_')
																			   .Replace('`', '_').Replace('~', '_')
																			   .Replace('#', '_').Replace('!', '_')
																			   .Replace('$', '_').Replace('%', '_')
																			   .Replace('^', '_').Replace('\'', '_').Replace(' ', '_').Replace('-', '_')
																			   .Replace('.', '_');
		}

		public static bool Contains(this string str, string[] strings)
		{
			bool contains = false;
			foreach(var item in strings)
			{
				if(str.Contains(item))
				{
					contains = true;
				}
			}

			return contains;
		}

		public static bool Contains(this string str, List<string> strings)
		{
			return str.Contains(strings.ToArray());
		}
	}
}
