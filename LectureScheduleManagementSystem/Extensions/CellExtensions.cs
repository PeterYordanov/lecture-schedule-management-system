﻿using DocumentFormat.OpenXml.Spreadsheet;
using NPOI.SS.UserModel;

namespace LectureScheduleManagementSystem.Extensions
{
	/// <summary>
	/// 
	/// </summary>
	static public class CellExtensions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cell"></param>
		/// <returns></returns>
		public static bool IsNull(this ICell cell)
		{
			return cell == null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cell"></param>
		/// <returns></returns>
		public static bool IsNull(this Cell cell)
		{
			return cell == null;
		}
	}
}
