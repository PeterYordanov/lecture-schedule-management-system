﻿using System;
using System.Collections.Generic;

namespace LectureScheduleManagementSystem.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Group
    {
        /// <value></value>
        public List<string> Names { get; set; }

        /// <value></value>
        public CellCoordinates CellCoordinates { get; set; }
    }
}
