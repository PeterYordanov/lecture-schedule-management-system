﻿namespace LectureScheduleManagementSystem.Models
{
	public class Cell
	{
		/// <value></value>
		public CellCoordinates CellCoordinates { get; set; }

		/// <value></value>
		public string Value { get; set; }

		public Cell(int row, int column, string value)
		{
			CellCoordinates = new CellCoordinates(row, column);
			Value = value;
		}

		public Cell(CellCoordinates cellCoordinates, string value)
		{
			CellCoordinates = cellCoordinates;
			Value = value;
		}
	}
}
