﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LectureScheduleManagementSystem.Models
{
    public abstract class BaseModel : INotifyPropertyChanged
    {
        protected void SetProperty<T>(ref T field, T value, [CallerMemberName] string propName = null)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                var pc = PropertyChanged;

                if (pc != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propName));
                }

                IsDirty = true;
            }
        }

        //Property that track if change to any property is made
        protected bool IsDirty { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
