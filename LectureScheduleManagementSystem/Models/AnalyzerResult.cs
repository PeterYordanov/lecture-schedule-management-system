﻿namespace LectureScheduleManagementSystem.Models
{
	public class AnalyzerResult
	{
		public string ErrorType { get; set; }

		public string Message { get; set; }

		public string Current { get; set; }

		public string Expected { get; set; }
	}
}
