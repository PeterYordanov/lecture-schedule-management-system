﻿using System.Globalization;
using CellType = NPOI.SS.UserModel.CellType;
using LectureScheduleManagementSystem.Extensions;
using NPOI.SS.UserModel;
using LectureScheduleManagementSystem.Models;
using System.IO;

namespace LectureScheduleManagementSystem.FileFormats
{
    /// <summary>
    /// 
    /// </summary>
    /// <example>
    /// <code>
    /// if(test.IsTest())
    /// </code>
    /// </example>
    public class ExcelFileReader
    {
        public IWorkbook Workbook { get; private set; }
        public DataFormatter DataFormatter { get; private set; }
        public IFormulaEvaluator FormulaEvaluator { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excelFileStream"></param>
        public ExcelFileReader(Stream excelFileStream)
        {
            Workbook = WorkbookFactory.Create(excelFileStream);
            if (Workbook != null)
            {
                DataFormatter = new DataFormatter(CultureInfo.InvariantCulture);
                FormulaEvaluator = WorkbookFactory.CreateFormulaEvaluator(Workbook);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workbook"></param>
        public void SetWorkbook(IWorkbook workbook)
        {
            Workbook = workbook;
        }

        public void Close()
        {
            Workbook.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cellCoordinates"></param>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public string ReadCell(CellCoordinates cellCoordinates, string sheetName)
        {
            return GetFormattedValue(Workbook.GetSheet(sheetName).GetRow(cellCoordinates.Row)
                                                                 .GetCell(cellCoordinates.Column));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public int GetRowCount(string sheetName)
        {
            return Workbook.GetSheet(sheetName).LastRowNum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public int GetColumnCount(string sheetName)
        {
            return Workbook.GetSheet(sheetName).GetRow(0).LastCellNum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public string GetFormattedValue(ICell cell)
        {
            string returnValue = string.Empty;
            if (!cell.IsNull())
            {
                try
                {
                    returnValue = DataFormatter.FormatCellValue(cell, FormulaEvaluator);
                }
                catch
                {
                    if (cell.CellType == CellType.Formula)
                    {
                        switch (cell.CachedFormulaResultType)
                        {
                            case CellType.String:
                                returnValue = cell.StringCellValue;
                                cell.SetCellValue(cell.StringCellValue);
                                break;
                            case CellType.Numeric:
                                returnValue = DataFormatter.FormatRawCellContents
                                (cell.NumericCellValue, 0, cell.CellStyle.GetDataFormatString());
                                cell.SetCellValue(cell.NumericCellValue);
                                break;
                            case CellType.Boolean:
                                returnValue = cell.BooleanCellValue.ToString();
                                cell.SetCellValue(cell.BooleanCellValue);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return (returnValue ?? string.Empty).Trim();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public string GetUnformattedValue(ICell cell)
        {
            string returnValue = string.Empty;
            if (!cell.IsNull())
            {
                try
                {
                    returnValue = (cell.CellType == CellType.Numeric ||
                    (cell.CellType == CellType.Formula &&
                    cell.CachedFormulaResultType == CellType.Numeric)) ?
                        FormulaEvaluator.EvaluateInCell(cell).NumericCellValue.ToString() :
                        this.DataFormatter.FormatCellValue(cell, FormulaEvaluator);
                }
                catch
                {
                    if (cell.CellType == CellType.Formula)
                    {
                        switch (cell.CachedFormulaResultType)
                        {
                            case CellType.String:
                                returnValue = cell.StringCellValue;
                                cell.SetCellValue(cell.StringCellValue);
                                break;
                            case CellType.Numeric:
                                returnValue = cell.NumericCellValue.ToString();
                                cell.SetCellValue(cell.NumericCellValue);
                                break;
                            case CellType.Boolean:
                                returnValue = cell.BooleanCellValue.ToString();
                                cell.SetCellValue(cell.BooleanCellValue);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return (returnValue ?? string.Empty).Trim();
        }
    }
}
