﻿using GemBox.Spreadsheet;
using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.Models;
using NPOI.SS.UserModel;
using System.Globalization;
using System.IO;

namespace LectureScheduleManagementSystem.FileFormats
{
	/// <summary>
	/// 
	/// </summary>
	/// <example>
	/// <code>
	/// </code>
	/// </example>
	public class ExcelFileWriter
	{
		public IWorkbook Workbook { get; private set; }
		public DataFormatter DataFormatter { get; private set; }
		public IFormulaEvaluator FormulaEvaluator { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="excelFileStream"></param>
		public ExcelFileWriter(Stream excelFileStream)
		{
			Workbook = NPOI.SS.UserModel.WorkbookFactory.Create(excelFileStream);
			if (Workbook != null)
			{
				DataFormatter = new DataFormatter(CultureInfo.InvariantCulture);
				FormulaEvaluator = WorkbookFactory.CreateFormulaEvaluator(Workbook);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cell"></param>
		/// <returns></returns>
		public string GetFormattedValue(ICell cell)
		{
			string returnValue = string.Empty;
			if (!cell.IsNull())
			{
				try
				{
					returnValue = DataFormatter.FormatCellValue(cell, FormulaEvaluator);
				}
				catch
				{
					if (cell.CellType == CellType.Formula)
					{
						switch (cell.CachedFormulaResultType)
						{
							case CellType.String:
								returnValue = cell.StringCellValue;
								cell.SetCellValue(cell.StringCellValue);
								break;
							case CellType.Numeric:
								returnValue = DataFormatter.FormatRawCellContents
								(cell.NumericCellValue, 0, cell.CellStyle.GetDataFormatString());
								cell.SetCellValue(cell.NumericCellValue);
								break;
							case CellType.Boolean:
								returnValue = cell.BooleanCellValue.ToString();
								cell.SetCellValue(cell.BooleanCellValue);
								break;
							default:
								break;
						}
					}
				}
			}

			return (returnValue ?? string.Empty).Trim();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cell"></param>
		/// <returns></returns>
		public string GetUnformattedValue(ICell cell)
		{
			string returnValue = string.Empty;
			if (!cell.IsNull())
			{
				try
				{
					returnValue = (cell.CellType == CellType.Numeric ||
					(cell.CellType == CellType.Formula &&
					cell.CachedFormulaResultType == CellType.Numeric)) ?
						FormulaEvaluator.EvaluateInCell(cell).NumericCellValue.ToString() :
						this.DataFormatter.FormatCellValue(cell, FormulaEvaluator);
				}
				catch
				{
					if (cell.CellType == CellType.Formula)
					{
						switch (cell.CachedFormulaResultType)
						{
							case CellType.String:
								returnValue = cell.StringCellValue;
								cell.SetCellValue(cell.StringCellValue);
								break;
							case CellType.Numeric:
								returnValue = cell.NumericCellValue.ToString();
								cell.SetCellValue(cell.NumericCellValue);
								break;
							case CellType.Boolean:
								returnValue = cell.BooleanCellValue.ToString();
								cell.SetCellValue(cell.BooleanCellValue);
								break;
							default:
								break;
						}
					}
				}
			}

			return (returnValue ?? string.Empty).Trim();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="workbook"></param>
		public void SetWorkbook(IWorkbook workbook)
		{
			Workbook = workbook;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cellCoordinates"></param>
		/// <param name="sheetName"></param>
		/// <returns></returns>
		public void WriteCell(Cell cellInput, string sheetName)
		{
			ISheet sheet = Workbook.GetSheet(sheetName) ?? Workbook.CreateSheet(sheetName);
			IRow dataRow = sheet.GetRow(cellInput.CellCoordinates.Row) ?? sheet.CreateRow(cellInput.CellCoordinates.Row);
			ICell cell = dataRow.GetCell(cellInput.CellCoordinates.Column) ?? dataRow.CreateCell(cellInput.CellCoordinates.Column);
			cell.CellStyle.Alignment = HorizontalAlignment.Center;
			cell.CellStyle.VerticalAlignment = VerticalAlignment.Center;
			cell.CellStyle.WrapText = true;

			cell.SetCellValue(cellInput.Value);
		}

		public void SaveFile(string filePath)
		{
			using (FileStream str = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
			{
				Workbook.Write(str);
			}
		}

		public MemoryStream SaveToStream()
		{
			MemoryStream str = new MemoryStream();
			Workbook.Write(str);
			return str;
		}

		public void Close()
		{
			//Stream.Close();
			Workbook.Close();
		}

		public void GetFormattedValue(ICell cell, string cellValue)
		{
			string returnValue = string.Empty;
			if (!cell.IsNull())
			{
				try
				{
					returnValue = DataFormatter.FormatCellValue(cell, FormulaEvaluator);
				}
				catch
				{
					if (cell.CellType == CellType.Formula)
					{
						switch (cell.CachedFormulaResultType)
						{
							case CellType.String:
								returnValue = cell.StringCellValue;
								cell.SetCellValue(cell.StringCellValue);
								break;
							case CellType.Numeric:
								returnValue = DataFormatter.FormatRawCellContents
								(cell.NumericCellValue, 0, cell.CellStyle.GetDataFormatString());
								cell.SetCellValue(cell.NumericCellValue);
								break;
							case CellType.Boolean:
								returnValue = cell.BooleanCellValue.ToString();
								cell.SetCellValue(cell.BooleanCellValue);
								break;
							default:
								break;
						}
					}
				}
			}

			cell.SetCellValue(cellValue);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sheetName"></param>
		/// <returns></returns>
		public int GetRowCount(string sheetName)
		{
			return Workbook.GetSheet(sheetName).LastRowNum;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sheetName"></param>
		/// <returns></returns>
		public int GetColumnCount(string sheetName)
		{
			return Workbook.GetSheet(sheetName).GetRow(0).LastCellNum;
		}
	}
}
