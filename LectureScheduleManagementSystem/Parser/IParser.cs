﻿using LectureScheduleManagementSystem.Models;
using System.Collections.Generic;

namespace LectureScheduleManagementSystem.Parser
{
	/// <summary>
	/// 
	/// </summary>
	interface IParser
	{
		List<ExcelColumn> Parse(string filePath);
	}
}
