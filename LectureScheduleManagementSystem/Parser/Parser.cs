﻿using NPOI.SS.UserModel;
using System.IO;
using LectureScheduleManagementSystem.FileFormats;
using System.Collections.Generic;
using LectureScheduleManagementSystem.Models;
using System;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading.Tasks;

namespace LectureScheduleManagementSystem.Parser
{
	/// <summary>
	/// 
	/// </summary>
	public class ExcelParser : IParser
	{
		public List<ExcelColumn> ColumnList { get; private set; }
		private object _lock = new object();

		public ExcelParser()
		{
			ColumnList = new List<ExcelColumn>();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public List<ExcelColumn> Parse(string filePath)
		{
			ParallelOptions options = new ParallelOptions { MaxDegreeOfParallelism = 8 };

			using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
			{
				ExcelFileReader excelFileReader = new ExcelFileReader(file);

				ISheet sheet = excelFileReader.Workbook.GetSheet("All");
				Parallel.For(0, excelFileReader.GetColumnCount("All"), options, column =>
				{
					ExcelColumn currentColumn = new ExcelColumn
					{
						FilePath = filePath,
						Lecturer = new Lecturer(),
						Subject = new Subject
						{
							SubjectCells = new List<SubjectCell>(),
							DateCells = new Dictionary<DateTime, int>(),
							LecturerNames = new List<string>()
						},
						Group = new Models.Group { Names = new List<string>() }
					};

					if (column < 3)
						return;

					Parallel.For(0, excelFileReader.GetRowCount("All") + 1, options, row =>
					{
						CellCoordinates currentCellCoordinates = new CellCoordinates(row, column);

						string currentCellValue = excelFileReader.ReadCell(currentCellCoordinates, "All");

						if (currentCellCoordinates.Row == 0 && currentCellCoordinates.Column >= 3) //Module Name
						{
							if (string.IsNullOrEmpty(currentCellValue))
							{
								return;
							}
							currentColumn.Subject.CellCoordinates = new CellCoordinates(row, column);
							currentColumn.Subject.Name = currentCellValue;
						}
						else if (currentCellCoordinates.Row == 1 && currentCellCoordinates.Column >= 3) //Lecturer name
						{
							if (string.IsNullOrEmpty(currentCellValue))
							{
								return;
							}
							currentColumn.Lecturer.Name = currentCellValue;
							currentColumn.Lecturer.CellCoordinates = new CellCoordinates(row, column);

							if (currentCellValue.Contains(","))
							{
								string[] lecturerNames = currentCellValue.Split(',');
								foreach (var names in lecturerNames)
								{
									currentColumn.Subject.LecturerNames.Add(names.Trim());
								}
							}
							else
							{
								currentColumn.Subject.LecturerNames.Add(currentCellValue.Trim());
							}
						}
						else if (currentCellCoordinates.Row == 3 && currentCellCoordinates.Column >= 3) //Group name
						{
							foreach (string groupName in currentCellValue.Split(','))
							{
								currentColumn.Group.Names.Add(groupName.Trim());
							}
							currentColumn.Group.CellCoordinates = new CellCoordinates(row, column);
						}
						else if (currentCellCoordinates.Row >= 21 && currentCellCoordinates.Column >= 3)
						{
							//Get the date first
							CellCoordinates dateCellCoordinates = new CellCoordinates(currentCellCoordinates.Row, 2);
							string dateCellValue = excelFileReader.ReadCell(dateCellCoordinates, "All");
							string patternTime = @"\d{2}.\d{2}";
							string patternTime1 = @"\d{1}.\d{2}";
							string patternContactHours = @"\(([0-9]*?)\)";

							DateTime startingDateTime = DateTime.MinValue;
							DateTime endingDateTime = DateTime.MinValue;

							int contactHours = 0;
							int HoursPerDay = 0;
							double MinutesPerDay = 0;
							if (!string.IsNullOrEmpty(currentCellValue) && currentCellValue.Contains('-', System.StringComparison.InvariantCulture))
							{
								string[] startingEndingTime = currentCellValue.Split('-');
								string startingTime = startingEndingTime[0];
								string endingTime = startingEndingTime[1];
								string contactHoursStr = currentCellValue;

								Match startingTimeMatch = Regex.Match(startingTime, patternTime, RegexOptions.IgnoreCase);
								Match startingTimeMatch1 = Regex.Match(startingTime, patternTime1, RegexOptions.IgnoreCase);
								Match contactHoursMatch = Regex.Match(contactHoursStr, patternContactHours, RegexOptions.IgnoreCase);

								Match endingTimeMatch = Regex.Match(endingTime, patternTime, RegexOptions.IgnoreCase);

								if (startingTimeMatch.Success)
								{
									startingTime = startingTimeMatch.Value;
									startingDateTime = DateTime.ParseExact(startingTime.Trim(), "HH.mm", CultureInfo.InvariantCulture);
								}
								else if (startingTimeMatch1.Success)
								{
									startingTime = startingTimeMatch1.Value;
									startingDateTime = DateTime.ParseExact(startingTime.Trim(), "H.mm", CultureInfo.InvariantCulture);
								}

								if (endingTimeMatch.Success)
								{
									endingTime = endingTimeMatch.Value;
									endingDateTime = DateTime.ParseExact(endingTime.Trim(), "HH.mm", CultureInfo.InvariantCulture);
								}

								if (contactHoursMatch.Success)
								{
									contactHoursStr = contactHoursMatch.Value;
									int removeLastIndex = contactHoursStr.IndexOf(')');
									int removeFirstIndex = contactHoursStr.IndexOf('(');

									contactHoursStr = contactHoursStr.Remove(removeLastIndex, 1);
									contactHoursStr = contactHoursStr.Remove(removeFirstIndex, 1);

									contactHours = int.Parse(contactHoursStr);
								}

								MinutesPerDay = endingDateTime.Subtract(startingDateTime).TotalMinutes;
								HoursPerDay = (int)endingDateTime.Subtract(startingDateTime).TotalMinutes / 60;
							}
							lock (_lock)
							{
								currentColumn.Subject.SubjectCells.Add(new SubjectCell
								{
									Date = DateTime.Parse(dateCellValue),
									ValueOfCell = currentCellValue,
									StartingHour = startingDateTime,
									EndingHour = endingDateTime,
									CellCoordinates = dateCellCoordinates,
									HoursPerDay = HoursPerDay,
									MinutesPerDay = MinutesPerDay,
									ContactHours = contactHours
								});

								currentColumn.Subject.DateCells.Add(DateTime.Parse(dateCellValue), dateCellCoordinates.Row);
							}
						}
					}); //for loop row

					lock (_lock)
						ColumnList.Add(currentColumn);
				}); //for loop columns

				excelFileReader.Close();
				file.Close();
			}

			return ColumnList;
		}
	}
}
