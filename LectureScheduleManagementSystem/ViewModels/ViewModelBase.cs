﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace LectureScheduleManagementSystem.ViewModels
{
    public class ViewModelBase : IBaseViewModel
    {
        public ViewModelBase()
        {
            //MsgQueue = HELPERS.SnackbarMessageQueueHelper.GetInstance().MessageQueue;
        }

        private string title;
        private bool snackbarSuccess;
        private bool isBusy = false;

        /// <summary>
        /// Property that track if change to any property is made
        /// </summary>
        private bool isDirty;
        protected bool IsDirty
        {
            get { return isDirty; }
            set
            {
                isDirty = value;
                OnPropertyChanged(nameof(IsDirty));
                //SetProperty(ref isDirty, value);
            }
        }

        /// <summary>
        /// View (Window) title property
        /// </summary>
        public string Title
        {
            get { return title; }
            protected set { SetProperty(ref title, value); }
        }

        /// <summary>
        /// Snackbar success flag (snackbar text color is set depending on this flag)
        /// </summary>
        public bool SnackbarSuccess
        {
            get { return snackbarSuccess; }
            set { SetProperty(ref snackbarSuccess, value); }
        }


        /// <summary>
        /// Flag indicating is busy status
        /// </summary>
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        public Action CloseAction { get; set; }
        public Action<string, string> ShowPopupMessage { get; set; }
        public Func<string, string, bool> ShowConfirmMessage { get; set; }
        public Func<string, bool, string, string[]> ShowOpenFileDialog { get; set; }
        public Func<Environment.SpecialFolder, bool, string> ShowFolderBrowserDialog { get; set; }

        #region INotifyPropertyChanged implementation

        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propName = null)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                var pc = PropertyChanged;
                if (pc != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propName));
                }
                IsDirty = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
