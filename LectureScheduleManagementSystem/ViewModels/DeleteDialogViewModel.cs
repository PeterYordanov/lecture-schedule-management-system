﻿using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.FileFormats;
using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.Logging;
using LectureScheduleManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace LectureScheduleManagementSystem.ViewModels
{
	public class DeleteDialogViewModel : ViewModelBase
	{
		private ICommand deleteCommand;
		public ICommand DeleteCommand
		{
			get { return deleteCommand; }
			set
			{
				deleteCommand = value;
				OnPropertyChanged(nameof(DeleteCommand));
			}
		}

		private ObservableCollection<string> subjects;
		public ObservableCollection<string> Subjects
		{
			get { return subjects; }
			set
			{
				subjects = value;
				OnPropertyChanged(nameof(Subjects));
			}
		}

		private ObservableCollection<string> groups;
		public ObservableCollection<string> Groups
		{
			get { return groups; }
			set
			{
				groups = value;
				OnPropertyChanged(nameof(Groups));
			}
		}

		private string selectedGroup;
		public string SelectedGroup
		{
			get { return selectedGroup; }
			set
			{
				selectedGroup = value;
				OnPropertyChanged(nameof(SelectedGroup));
				BindSubjects();
			}
		}

		private string selectedSubject;
		public string SelectedSubject
		{
			get { return selectedSubject; }
			set
			{
				selectedSubject = value;
				OnPropertyChanged(nameof(SelectedSubject));
			}
		}

		public MainViewModel MainViewModel { get; set; }
		private ObservableCollection<Parent> treeViewValues = new ObservableCollection<Parent>();

		public DeleteDialogViewModel(MainViewModel mainViewModel)
		{
			MainViewModel = mainViewModel;
			BindGroups();
			BindTreeViewValues();
			deleteCommand = new RelayCommand(p => Delete(), p => CommandCanExecute());
		}

		public void BindGroups()
		{
			try
			{
				ObservableCollection<string> groups = new ObservableCollection<string>();
				foreach (var item in MainViewModel.TreeViewItems)
				{
					groups.Add(item.Name);
				}
				Groups = groups;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"Binding groups in DeleteDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindSubjects()
		{
			try
			{
				ObservableCollection<string> subjects = new ObservableCollection<string>();
				foreach (var subject in MainViewModel.TreeViewItems.Where(x => x.Name == SelectedGroup))
				{
					foreach (var item in subject.ChildItems)
					{
						subjects.Add(item.Name);
					}
				}
				Subjects = subjects;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"Binding subjects in DeleteDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindTreeViewValues()
		{
			foreach (var item in MainViewModel.TreeViewItems)
			{
				foreach (var subject in item.ChildItems)
				{
					treeViewValues.Add(new Parent(subject.Name));
				}
			}

			treeViewValues = treeViewValues.DistinctBy(x => x.Name);
		}

		public bool CommandCanExecute()
		{
			return !string.IsNullOrEmpty(SelectedSubject);
		}

		public void Delete()
		{
			try
			{
				List<ExcelColumn> columns = MainViewModel.ExcelColumns.Where(x => SelectedSubject.Contains(x.Subject.Name)).ToList();

				ExcelFileWriter writer = MainViewModel.Writer;

				if (SelectedSubject.Contains(columns.FirstOrDefault().Subject.Name))
				{
					//Update ExcelColumns (Used for fetching data)
					foreach (var item in columns)
					{
						item.Group.Names.Clear();
						item.Lecturer.Name = string.Empty;
						item.Subject.Name = string.Empty;

						foreach (var subject in item.Subject.SubjectCells)
						{
							subject.ContactHours = 0;
							subject.Date = DateTime.MinValue;
							subject.StartingHour = DateTime.MinValue;
							subject.EndingHour = DateTime.MinValue;
							subject.HoursPerDay = 0;
							subject.MinutesPerDay = 0;
							subject.ValueOfCell = string.Empty;
						}
					}

					//Update UI
					foreach (var group in MainViewModel.TreeViewItems)
					{
						List<TreeItem> itemsToRemove = group.ChildItems.Where(x => x.Name == SelectedSubject).ToList();
						foreach (var item in itemsToRemove)
						{
							group.ChildItems.Remove(item);
						}
					}

					//Update excel file
					foreach (var item in columns)
					{
						writer.WriteCell(new Cell(new CellCoordinates(0, item.Group.CellCoordinates.Column), string.Empty), "All");
						writer.WriteCell(new Cell(new CellCoordinates(1, item.Group.CellCoordinates.Column), string.Empty), "All");
						writer.WriteCell(new Cell(new CellCoordinates(3, item.Group.CellCoordinates.Column), string.Empty), "All");

						for(int rowIndex = 0; rowIndex <= writer.GetRowCount("All"); rowIndex++)
						{
							CellCoordinates cellCoordinates = new CellCoordinates(rowIndex, item.Group.CellCoordinates.Column);
							writer.WriteCell(new Cell(cellCoordinates, string.Empty), "All");
						}
					}
				}

				Subjects.Clear();
				BindSubjects();
				MessageBoxHelpers.ShowInformation(Constants.DeleteMessage, Constants.DeleteCaption);
				MainViewModel.Logger.LogInfo(new LogModel
				{
					Message = $"Delete success!",
					EventType = EventType.Success,
					LogType = LogType.Information
				});
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"Deleting in DeleteDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}
	}
}