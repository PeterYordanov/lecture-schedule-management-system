﻿namespace LectureScheduleManagementSystem.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogType
    {
        Information,
        Error,
        Warning,
        Debug,
        Fatal
    }
}
