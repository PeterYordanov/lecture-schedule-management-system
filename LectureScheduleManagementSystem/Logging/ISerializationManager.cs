﻿namespace LectureScheduleManagementSystem.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISerializationManager
    {
        string Serialize(object input);
        T Deserialize<T>(string input);
    }
}
