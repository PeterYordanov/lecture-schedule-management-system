﻿namespace LectureScheduleManagementSystem.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILogger
    {
        void LogInfo<T>(T obj);

        void LogError<T>(T obj);

        void LogWarning<T>(T obj);

        void LogFatal<T>(T obj);

        void LogDebug<T>(T obj);
    }
}
