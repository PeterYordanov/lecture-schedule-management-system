﻿
using System;

namespace LectureScheduleManagementSystem.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public static class DateTimeHelpers
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static string GetDateLong(string date)
		{
			return DateTime.Parse(date).ToString("dd MMMM yyyy");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static string GetDateShort(string date)
		{
			return DateTime.Parse(date).ToString("dd.MM.yyyy");
		}
	}
}
