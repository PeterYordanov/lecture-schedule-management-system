﻿namespace LectureScheduleManagementSystem.Helpers
{
	static public class Constants
	{
		public const string AddCaption = "Created";
		public const string EditCaption = "Modified";
		public const string DeleteCaption = "Deleted";
		public const string SubjectCaption = "Subject";
		public const string WarningCaption = "Warning";
		public const string AddMessage = "Subject added, please export or save changes!";
		public const string EditMessage = "Subject edited, please export or save changes!";
		public const string DeleteMessage = "Subject deleted, please export or save changes!";
		public const string SubjectMessage = "Please, change your subject abbreviation.";
		public const string KeyMaxDegreeOfParallelism = "MaxDegreeOfParallelism";
		public const string FileNotImportedMessage = "A file needs to be imported first!";
		public const string ElevationErrorMessage = "Please, run Lecture Schedule Management System as Administrator";
		public const string RequiredElevation = "Required Elevation!";
		public static string[] DaysOfTheWeek = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
	}
}
