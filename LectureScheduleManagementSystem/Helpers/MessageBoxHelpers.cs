﻿using System;
using System.Windows;
using MessageBox = HandyControl.Controls.MessageBox;

namespace LectureScheduleManagementSystem.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	static class MessageBoxHelpers
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="caption"></param>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static void ShowInformation(string text, string caption = "Information")
		{
			MessageBox.Show(text, caption, MessageBoxButton.OK, MessageBoxImage.Information);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="caption"></param>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static void ShowError(string text, string caption = "Error")
		{
			MessageBox.Show(text, caption, MessageBoxButton.OK, MessageBoxImage.Error);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="caption"></param>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static void ShowWarning(string text, string caption = "Warning")
		{
			MessageBox.Show(text, caption, MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="caption"></param>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static void ShowExclamation(string text, string caption = "")
		{
			MessageBox.Show(text, caption, MessageBoxButton.OK, MessageBoxImage.Exclamation);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="text"></param>
		/// <param name="caption"></param>
		/// <param name="yesAction"></param>
		/// <param name="noAction"></param>
		/// <example>
		/// <code>
		/// </code>
		/// </example>
		public static void ShowQuestion<T>(string text, string caption, Func<T> yesAction, Func<T> noAction)
		{
			MessageBoxResult result = MessageBox.Show(text, caption, MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

			if(result == MessageBoxResult.Yes)
			{
				yesAction?.Invoke();
			}
			else if(result == MessageBoxResult.No)
			{
				noAction?.Invoke();
			}
		}

		public static bool VerifyFields(string subjectName, string groupName, string lecturerName)
		{
			bool exit = false;
			if(string.IsNullOrEmpty(subjectName))
			{
				ShowExclamation("Don't leave the 'Subject Name' field empty!");
				exit = true;
			}

			if (string.IsNullOrEmpty(groupName))
			{
				ShowExclamation("Don't leave the 'Group Name' field empty!");
				exit = true;
			}

			if (string.IsNullOrEmpty(lecturerName))
			{
				ShowExclamation("Don't leave the 'Lecturer Name' field empty!");
				exit = true;
			}

			return exit;
		}
	}
}
