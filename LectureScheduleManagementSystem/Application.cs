﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LectureScheduleManagementSystem
{
    public enum ApplicationWindow
    {
        AddDialog,
        EditDialog,
        DeleteDialog,
        MainWindow
    }
}
