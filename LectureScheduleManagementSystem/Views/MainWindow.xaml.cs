﻿using Fluent;
using LectureScheduleManagementSystem.FileFormats;
using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.ViewModels;
using System;
using System.IO;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using MessageBox = System.Windows.MessageBox;

namespace LectureScheduleManagementSystem.Views
{
	public partial class MainWindow : RibbonWindow
	{
		private ExcelFileWriter Writer { get; set; }

		public MainWindow()
		{
			InitializeComponent();
			MainViewModel vm = new MainViewModel();

			if (vm.CloseAction == null)
			{
				vm.CloseAction = new Action(() => this.Close());
			}
			if (vm.ShowPopupMessage == null)
			{
				vm.ShowPopupMessage = new Action<string, string>((text, header) => MessageBox.Show(text, header));

			}
			this.DataContext = vm;
			Width = Screen.PrimaryScreen.Bounds.Width;
			Height = Screen.PrimaryScreen.Bounds.Height;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private async void OnLoaded(object sender, RoutedEventArgs e)
		{
			try
			{
				ContinueButton.Visibility = Visibility.Hidden;
				ProgressBar.Visibility = Visibility.Visible;
				//Color color = (Color)ColorConverter.ConvertFromString("#3473b9");
				//ProgressBar.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B));
				ProgressBar.Foreground = Brushes.Gray;

				string saveFilesDir = "SaveFiles";
				string logOutputDir = "LogOutput";
				string outputDir = "Output";

				//CleanDir(saveFilesDir);
				System.IO.DirectoryInfo directory = new DirectoryInfo(logOutputDir);

				if (directory.Exists)
				{
					if (directory.GetFiles().Length >= 30)
					{
						foreach (FileInfo file in directory.GetFiles())
						{
							file.Delete();
						}
						foreach (DirectoryInfo dir in directory.GetDirectories())
						{
							dir.Delete(true);
						}

						directory.Delete(true);
					}
				}
				CleanDir(outputDir);

				CreateDir(saveFilesDir);
				CreateDir(logOutputDir);
				CreateDir(outputDir);

				bool isElevated = false;

				using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
				{
					WindowsPrincipal principal = new WindowsPrincipal(identity);
					isElevated = principal.IsInRole(WindowsBuiltInRole.Administrator);
				}

				for (int i = 0; i <= 30; i++)
				{
					await Task.Delay(0x32 << (0x01) / Environment.ProcessorCount).ConfigureAwait(true);
					ProgressBar.Value = ((i * 0x19 << 0x02) / 0x1e);
					if (ProgressBar.Value == 0x2b)
					{
						await Task.Delay(((0x5C4 << 0x03) >> 0x01) & 0x2B5C / (Environment.ProcessorCount * 0x02)).ConfigureAwait(true);
					}
				}

				if (!isElevated)
				{
					MessageBoxHelpers.ShowError(Constants.ElevationErrorMessage, Constants.RequiredElevation);
					Close();
				}

				ProgressBar.Visibility = Visibility.Hidden;
				ContinueButton.Visibility = Visibility.Visible;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
			}
		}

		private static void CreateDir(string dirName)
		{
			if (!Directory.Exists(dirName))
				Directory.CreateDirectory(dirName);
		}

		private static void CleanDir(string dirName)
		{
			System.IO.DirectoryInfo directory = new DirectoryInfo(dirName);

			if (directory.Exists)
			{
				foreach (FileInfo file in directory.GetFiles())
				{
					file.Delete();
				}
				foreach (DirectoryInfo dir in directory.GetDirectories())
				{
					dir.Delete(true);
				}

				directory.Delete(true);
			}
		}
	}
}
